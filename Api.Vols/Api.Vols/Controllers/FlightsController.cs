﻿using Api.Vols.Datas.Entities;
using Api.Vols.Datas.Repository;
using LiteDB;
using Microsoft.AspNetCore.Mvc;

namespace Api.Vols.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightsController : ControllerBase
    {

        /// <summary>
        /// The flight repository
        /// </summary>
        private readonly IFlightRepository _flightRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlightsController"/> class.
        /// </summary>
        /// <param name="flightRepository">The flight repository.</param>
        public FlightsController(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }

        // Endpoint GET pour récupérer la liste de tous les vols
        [HttpGet]
        [ProducesResponseType(typeof(List<Flight>), 200)]
        public IActionResult GetFlights()
        {
            return Ok(_flightRepository.GetFlights());
        }

        // Endpoint GET pour récupérer un vol par son identifiant
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Flight), 200)]
        public IActionResult GetFlightById(string id)
        {
            var flight = _flightRepository.GetFlightById(new ObjectId(id));

            if (flight == null)
            {
                return NotFound();
            }

            return Ok(flight);
        }

        // Endpoint POST pour créer un nouveau vol
        [HttpPost]
        [ProducesResponseType(typeof(Flight), 200)]
        public IActionResult CreateFlight([FromBody] Flight flight)
        {
            return Ok(_flightRepository.CreateFlight(flight));
        }

        // Endpoint PUT pour mettre à jour un vol par son identifiant
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Flight), 200)]
        public IActionResult UpdateFlightById(string id, [FromBody] Flight flight)
        {
            var checkFlight = _flightRepository.GetFlightById(new ObjectId(id));

            if (checkFlight == null)
            {
                return NotFound();
            }

            return Ok(_flightRepository.UpdateFlight(flight));
        }


        // Endpoint DELETE pour supprimer un vol par son identifiant
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult DeleteFlight(string id)
        {
            var checkFlight = _flightRepository.GetFlightById(new ObjectId(id));

            if (checkFlight == null)
            {
                return NotFound();
            }

            var success = _flightRepository.DeleteFlight(new ObjectId(id));
            return Ok(success);
        }


        // Endpoint GET pour récupérer le statut d'un siège sur un vol spécifique
        [HttpGet("{numeroVol}/siege/{nomSiege}")]
        [ProducesResponseType(typeof(Seat), 200)]
        public IActionResult GetSeatStatus(string numeroVol, string nomSiege)
        {
            var seat = _flightRepository.GetSeatStatus(numeroVol, nomSiege);

            if (seat == null)
            {
                return NotFound();
            }

            return Ok(seat);
        }

        [HttpPut("{numeroVol}/siege/{nomSiege}/{newNomSiege}/{status}")]
        [ProducesResponseType(typeof(Seat), 200)]
        public IActionResult UpdateSeatStatus(string numeroVol, string nomSiege, string newNomSiege, string status)
        {
            var seat = _flightRepository.UpdateSeatStatus(numeroVol, nomSiege, newNomSiege, status);

            if (seat == null)
            {
                return NotFound();
            }

            return Ok(seat);
        }
    }
}
