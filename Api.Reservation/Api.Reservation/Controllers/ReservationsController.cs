﻿using Api.Reservation.Business.Models;
using Api.Reservation.Business.Service;
using Microsoft.AspNetCore.Mvc;

namespace Api.Reservation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        /// <summary>
        /// The reservation service
        /// </summary>
        private readonly IReservationService _reservationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservationsController"/> class.
        /// </summary>
        /// <param name="reservationService">The reservation service.</param>
        public ReservationsController(IReservationService reservationService)
        {
            _reservationService = reservationService;
        }

        // Endpoint POST pour créer une nouvelle réservation
        [HttpPost]
        [ProducesResponseType(typeof(Datas.Entities.Reservation), 200)]
        public async Task<IActionResult> CreateReservationAsync([FromBody] CreateReservationDto reservation)
        {
            try {
                var reservationToCreate = new Datas.Entities.Reservation()
                {
                    UtilisateurId = reservation.UtilisateurId,
                    NumeroVol = reservation.NumeroVol,
                    NumeroSiege = reservation.NumeroSiege,
                    Statut = Datas.Entities.StatutReservation.Active,
                    Changement = DateTime.Now
                };

                return Ok(await _reservationService.CreateReservationAsync(reservationToCreate));
            }
            catch (Exception e) { return Problem(e.Message, e.StackTrace); }
        }

        // Endpoint GET pour récupérer la liste des réservations
        [HttpGet]
        [ProducesResponseType(typeof(List<Datas.Entities.Reservation>), 200)]
        public async Task<IActionResult> GetReservationsAsync()
        {
            return Ok(await _reservationService.GetReservationsAsync());
        }

        // Endpoint DELETE pour annuler une reservation
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(String), 200)]
        public async Task<IActionResult> DeleteReservationAsync(int id)
        {
            await _reservationService.DeleteReservationAsync(id);

            return Ok("OK");
        }




        // Endpoint GET pour récupérer la liste des réservations
        [HttpGet("name/{name}")]
        [ProducesResponseType(typeof(List<Datas.Entities.Reservation>), 200)]
        public async Task<IActionResult> GetReservationsByUserNameAsync(string name)
        {
            return Ok(await _reservationService.GetReservationsByUtilisateurAsync(name));
        }

        // Endpoint GET pour récupérer la liste des réservations
        [HttpGet("volnumber/{volNumber}")]
        [ProducesResponseType(typeof(List<Datas.Entities.Reservation>), 200)]
        public async Task<IActionResult> GetReservationsByVolNumberAsync(string volNumber)
        {
            return Ok(await _reservationService.GetReservationsByNumeroVolAsync(volNumber));
        }

    }
}