﻿using Api.Reservation.Datas.Entities;
using Api.Reservation.Datas.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Reservation.Controllers
{
    [Route("api/utilisateurs")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private readonly IUtilisateurRepository _utilisateurRepository;

        public UtilisateurController(IUtilisateurRepository utilisateurRepository)
        {
            _utilisateurRepository = utilisateurRepository;
        }

        // Endpoint HTTP GET pour récupérer la liste des utilisateurs
        [HttpGet]
        [ProducesResponseType(typeof(List<Utilisateur>), 200)]
        public async Task<IActionResult> GetUtilisateursAsync()
        {
            return Ok(await _utilisateurRepository.GetUtilisateursAsync());
        }


        // Endpoint HTTP GET pour récupérer un utilisateur par son identifiant (ID)
        [HttpGet("{id}", Name = "ObtenirUtilisateur")]
        [ProducesResponseType(typeof(Utilisateur), 200)]
        public async Task<IActionResult> GetUtilisateurByIdAsync(int id)
        {
            var utilisateur = await _utilisateurRepository.GetUtilisateurByIdAsync(id);
            if (utilisateur == null)
            {
                return NotFound();
            }
            return Ok(utilisateur);
        }


        // Endpoint HTTP POST pour créer un nouvel utilisateur
        [HttpPost]
        [ProducesResponseType(typeof(Utilisateur), 200)]
        public async Task<IActionResult> CreateUtilisateurAsync([FromBody] Utilisateur utilisateur)
        {
            try
            {
                return Ok(await _utilisateurRepository.CreateUtilisateurAsync(utilisateur));
            }
            catch (Exception e)
            {
                return Problem(e.Message, e.StackTrace);
            }
        }


        // Endpoint HTTP PUT pour mettre à jour un utilisateur existant par son identifiant (ID)
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> UpdateUtilisateur(int id, [FromBody] Utilisateur utilisateurModifie)
        {
            var utilisateur = await _utilisateurRepository.GetUtilisateurByIdAsync(id);
            if (utilisateur == null)
            {
                return NotFound();
            }

            await _utilisateurRepository.UpdateUtilisateur(utilisateur);
            return NoContent();
        }


        // Endpoint HTTP DELETE pour supprimer un utilisateur par son identifiant (ID)
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> SupprimerUtilisateur(int id)
        {
            var utilisateur = await _utilisateurRepository.GetUtilisateurByIdAsync(id);
            if (utilisateur == null)
            {
                return NotFound();
            }

            await _utilisateurRepository.DeleteUtilisateurAsync(id);
            return NoContent();
        }
    }
}
