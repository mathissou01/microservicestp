﻿using Api.Reservation.Business.Models;
using Api.Reservation.Datas.Entities;
using Api.Reservation.Datas.Repository;
using Api.Reservation.Generals.Common;
using Refit;

namespace Api.Reservation.Business.Service
{
    public class ReservationService : IReservationService
    {
        /// <summary>
        /// The reservation repository
        /// </summary>
        private readonly IReservationRepository _reservationRepository;

        /// <summary>
        /// The utilisateur repository
        /// </summary>
        private readonly IUtilisateurRepository _utilisateurRepository;

        /// <summary>
        /// The flights API
        /// </summary>
        private readonly IFlightsApi _flightsApi;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReservationService"/> class.
        /// </summary>
        /// <param name="reservationRepository">The reservation repository.</param>
        /// <param name="utilisateurRepository">The utilisateur repository.</param>
        /// <param name="flightsApi">The flights API.</param>
        public ReservationService(IReservationRepository reservationRepository, IUtilisateurRepository utilisateurRepository, IFlightsApi flightsApi)
        {
            _reservationRepository = reservationRepository;
            _utilisateurRepository = utilisateurRepository;
            _flightsApi = flightsApi;
        }

        /// <summary>
        /// Cette méthode permet de faire un appel Http vers l'API des vols pour
        /// recupérer les informations d'un siege
        /// </summary>
        /// <param name="numeroVol">Le numéro du vol.</param>
        /// <param name="nomSiege">Le nom du siege</param>
        /// <returns></returns>
        public async Task<Seat> GetSiegeStatusAsync(string numeroVol, string nomSiege)
        {
            return await _flightsApi.GetSeatStatusAsync(numeroVol, nomSiege)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Creates the reservation asynchronous.
        /// </summary>
        /// <param name="reservation">The reservation.</param>
        /// <returns></returns>
        /// <exception cref="Api.Reservation.Generals.Common.BusinessException">Echec de création d'une reservation : Le siège n'est pas disponible.</exception>
        public async Task<Datas.Entities.Reservation> CreateReservationAsync(Datas.Entities.Reservation reservation)
        {
            // Vérifier l'existence de l'utilisateur ,sinon levez une exception
            //TODO

            var siegeStatus = await GetSiegeStatusAsync(reservation.NumeroVol, reservation.NumeroSiege);

            if (siegeStatus?.Status != "Disponible")
            {
                throw new BusinessException("Echec de création d'une reservation : Le siège n'est pas disponible.");
            }

            var resultReserve = await _flightsApi.UpdateSeatStatusAsync(reservation.NumeroVol, reservation.NumeroSiege, reservation.NumeroSiege, "Non-Disponible").ConfigureAwait(false);

            if (resultReserve == null)
            {
                throw new BusinessException("Echec de la reservation d'un siège : Le siège n'a pas pu être réservé.");
            }

            // Le siège est disponible, procédez à la création de la réservation
            return await _reservationRepository.CreateReservationAsync(reservation)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste des reservations
        /// </summary>
        /// <returns></returns>
        public async Task <List<Datas.Entities.Reservation>> GetReservationsAsync()
        {
            return await _reservationRepository.GetReservationsAsync()
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de faire un appel Http vers l'API des vols pour
        /// mettre à jour la disponibilité d'un siege
        /// </summary>
        /// <param name="numeroVol">Le numéro du vol.</param>
        /// <param name="nomSiege">Le nom du siege</param>
        /// <returns></returns>
        public async Task<Seat> UpdateSeatStatusAsync(string numeroVol, string nomSiege)
        {
            return await _flightsApi.UpdateSeatStatusAsync(numeroVol, nomSiege, nomSiege, "Occupé")
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de supprimer une reservation
        /// </summary>
        /// <returns></returns>
        public async Task DeleteReservationAsync(int id)
        {
            var reservation = await _reservationRepository.GetReservationByIdAsync(id);

            if (reservation == null)
            {
                throw new BusinessException("Echec de la supression de reservation");
            }

            await _flightsApi.UpdateSeatStatusAsync(reservation.NumeroVol, reservation.NumeroSiege, reservation.NumeroSiege, "Disponible");

            await _reservationRepository.DeleteReservationAsync(id)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de récupérer des vols par le numéro de vol
        /// </summary>
        /// <returns></returns>
        public async Task<List<Datas.Entities.Reservation>> GetReservationsByNumeroVolAsync(string numeroVol)
        {
            return await _reservationRepository.GetReservationsByNumeroVolAsync(numeroVol);
        }

        /// <summary>
        /// Cette méthode permet de récupérer des vols par nom d'utilisateur
        /// </summary>
        /// <returns></returns>
        public async Task<List<Datas.Entities.Reservation>> GetReservationsByUtilisateurAsync(string nomUtilisateur)
        {
            return await _reservationRepository.GetReservationsByUtilisateurAsync(nomUtilisateur);
        }
    }
}
