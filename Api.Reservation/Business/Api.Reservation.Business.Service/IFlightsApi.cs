﻿using Api.Reservation.Business.Models;
using Refit;

namespace Api.Reservation.Business.Service
{
    public interface IFlightsApi
    {
        [Get("/api/Flights/{numeroVol}/siege/{nomSiege}")]
        Task<Seat> GetSeatStatusAsync(string numeroVol, string nomSiege);

        [Put("/api/Flights/{numeroVol}/siege/{nomSiege}/{newNomSiege}/{status}")]
        Task<Seat> UpdateSeatStatusAsync(string numeroVol, string nomSiege, string newNomSiege, string status);
    }
}
