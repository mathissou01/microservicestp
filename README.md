# Microservices

# Microservices API Documentation

## Utilisateur Controller

### Get Utilisateurs

- **Endpoint**: `GET /api/utilisateurs`
- **Description**: Get a list of all users.
- **Response**: Returns a list of user objects.
- **HTTP Status Code**: 200 (OK)

### Get Utilisateur by ID

- **Endpoint**: `GET /api/utilisateurs/{id}`
- **Description**: Get a user by their ID.
- **Parameters**:
  - `id` (int): The ID of the user.
- **Response**: Returns a user object.
- **HTTP Status Code**: 200 (OK) if user is found, 404 (Not Found) if user does not exist.

### Create Utilisateur

- **Endpoint**: `POST /api/utilisateurs`
- **Description**: Create a new user.
- **Request Body**: User object in JSON format.
- **Example Request Body**: 
  {
    "id": 0,
    "nom": "string",
    "prenom": "string",
    "email": "string",
    "dateNaissance": "2023-11-03T16:46:39.061Z"
  }
- **Response**: Returns the created user object.
- **HTTP Status Code**: 200 (OK) if user is created, 400 (Bad Request) if there are validation errors, 500 (Internal Server Error) for other errors.

### Update Utilisateur

- **Endpoint**: `PUT /api/utilisateurs/{id}`
- **Description**: Update an existing user.
- **Parameters**:
  - `id` (int): The ID of the user to update.
- **Request Body**: User object with updated information in JSON format.
- **Example Request Body**: 
  {
    "id": 0,
    "nom": "string",
    "prenom": "string",
    "email": "string",
    "dateNaissance": "2023-11-03T16:46:04.781Z"
  }
- **Response**: Returns no content.
- **HTTP Status Code**: 204 (No Content) if user is updated, 404 (Not Found) if user does not exist.

### Delete Utilisateur

- **Endpoint**: `DELETE /api/utilisateurs/{id}`
- **Description**: Delete a user by their ID.
- **Parameters**:
  - `id` (int): The ID of the user to delete.
- **Response**: Returns no content.
- **HTTP Status Code**: 204 (No Content) if user is deleted, 404 (Not Found) if user does not exist.

## Reservations Controller

### Create Reservation

- **Endpoint**: `POST /api/Reservations`
- **Description**: Create a new reservation.
- **Request Body**: Reservation object in JSON format.
- **Example Request Body**: 
  {
    "utilisateurId": 0,
    "numeroVol": "string",
    "numeroSiege": "string"
  }
- **Response**: Returns the created reservation object.
- **HTTP Status Code**: 200 (OK) if reservation is created, 400 (Bad Request) if there are validation errors, 500 (Internal Server Error) for other errors.

### Get Reservations

- **Endpoint**: `GET /api/Reservations`
- **Description**: Get a list of all reservations.
- **Response**: Returns a list of reservation objects.
- **HTTP Status Code**: 200 (OK)

### Get Reservation by user name

- **Endpoint**: `GET /api/Reservations/name/{name}`
- **Description**: Get a list of reservations by the user's name.
- **Parameters**:
  - `name` (string): The name of the user.
- **Response**: Returns a list of reservation objects.
- **HTTP Status Code**: 200 (OK) if reservations are found for the user.

### Get Reservation by flight number

- **Endpoint**: `GET /api/Reservations/volnumber/{volNumber}`
- **Description**: Get a list of reservations by the flight number.
- **Parameters**:
  - `volNumber` (string): The flight number.
- **Response**: Returns a list of reservation objects.
- **HTTP Status Code**: 200 (OK) if reservations are found for the specified flight number.

### Delete Reservations

- **Endpoint**: `DELETE /api/Reservations/{id}`
- **Description**: Delete a reservation by its ID and update the status of the reserved seat to "Available."
- **Parameters**:
  - `id` (int): The ID of the reservation to be canceled.
- **Response**: Returns a status message.
- **Usage**: This method cancels a reservation, making the reserved seat available for booking again.


## Flights Controller

### Get Flights

- **Endpoint**: `GET /api/Flights`
- **Description**: Get a list of all flights.
- **Response**: Returns a list of flight objects.
- **HTTP Status Code**: 200 (OK)

### Get Flight by ID

- **Endpoint**: `GET /api/Flights/{id}`
- **Description**: Get a flight by its ID.
- **Parameters**:
  - `id` (string): The ID of the flight.
- **Response**: Returns a flight object.
- **HTTP Status Code**: 200 (OK) if flight is found, 404 (Not Found) if flight does not exist.

### Create Flight

- **Endpoint**: `POST /api/Flights`
- **Description**: Create a new flight.
- **Request Body**: Flight object in JSON format.
- **Example Request Body**: 
  {
    "id": {},
    "idToString": "string",
    "flightNumber": "string",
    "origin": "string",
    "destination": "string",
    "date": "2023-11-03T16:52:29.116Z",
    "sieges": [
      {
        "name": "string",
        "status": "string"
      }
    ],
    "informationAeroplane": {
      "modele": "string",
      "compagny": "string"
    }
  }
- **Response**: Returns the created flight object.
- **HTTP Status Code**: 200 (OK) if flight is created, 400 (Bad Request) if there are validation errors, 500 (Internal Server Error) for other errors.

### Update Flight

- **Endpoint**: `PUT /api/Flights/{id}`
- **Description**: Update an existing flight.
- **Parameters**:
  - `id` (string): The ID of the flight to update.
- **Request Body**: Flight object with updated information in JSON format.
- **Example Request Body**: 
  {
    "id": {},
    "idToString": "string",
    "flightNumber": "string",
    "origin": "string",
    "destination": "string",
    "date": "2023-11-03T16:52:29.116Z",
    "sieges": [
      {
        "name": "string",
        "status": "string"
      }
    ],
    "informationAeroplane": {
      "modele": "string",
      "compagny": "string"
    }
  }
- **Response**: Returns no content.
- **HTTP Status Code**: 204 (No Content) if flight is updated, 404 (Not Found) if flight does not exist.

### Delete Flight

- **Endpoint**: `DELETE /api/Flights/{id}`
- **Description**: Delete a flight by its ID.
- **Parameters**:
  - `id` (string): The ID of the flight to delete.
- **Response**: Returns no content.
- **HTTP Status Code**: 204 (No Content) if flight is deleted, 404 (Not Found) if flight does not exist.

### Get Seat Status

- **Endpoint**: `GET /api/Flights/{numeroVol}/siege/{nomSiege}`
- **Description**: Get the status of a seat on a flight.
- **Parameters**:
  - `numeroVol` (string): The flight number.
  - `nomSiege` (string): The seat name.
- **Response**: Returns the seat status.
- **HTTP Status Code**: 200 (OK) if seat is found, 404 (Not Found) if seat does not exist.

### UPDATE Seat Status (by reservation)

- **Endpoint**: `PUT /api/Flights/{numeroVol}/siege/{nomSiege}`
- **Description**: Update the status of a seat on a flight to "Occupied."
- **Parameters**:
  - `numeroVol` (string): The flight number.
  - `nomSiege` (string): The seat name.
- **Response**: Returns a Seat object with the updated status.
- **Usage**: This method is used to mark a seat as occupied for a specific flight.

### UPDATE Seat Status (by flights)

- **Endpoint**: `PUT /api/Flights/{numeroVol}/siege/{nomSiege}/{newNomSiege}/{status}`
- **Description**: Update the status of a seat on a flight.
- **Parameters**:
  - `numeroVol` (string): The flight number.
  - `nomSiege` (string): The seat name.
  - `newNomSiege` (string): The new seat name (same as the pevious one).
  - `status` (string): The seat new status.
- **Response**: Returns a Seat object with the updated status.
- **Usage**: This method is used to update a seat to a new status for a specific flight.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nextu-n3/niveau3/microservices.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/nextu-n3/niveau3/microservices/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
